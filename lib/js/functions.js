$(document).ready(function() {
    var patch = 'lib/php/';
    // Variável que indica o comando de logoff
    var logoff = false;
    //Variavel de controle de dialog
    var dialog ;
    var okDialog = false;

    //Guarda valor selecionado nas listas
    var selected_item;

    //Botões da barrade navegação lateral
    var nav_home = document.getElementById("nav-home");
    var nav_cto = document.getElementById("nav-cto");
    var nav_atendimento = document.getElementById("nav-atendimento");

    //Controles Atendimento
    var ate_busca = document.getElementById("ate-busca");
    var ate_cadastro = document.getElementById("ate-cadastro");

    //Controle da tela principal
    var form_home = document.getElementById("form-home");
    var form_cto = document.getElementById("form-cto");
    var form_atendimneto = document.getElementById("form-atendimento");

    //Controle do Login
    var btn_login_nav = document.getElementById("btn_login_nav");
    var lbl_usuario_logado = document.getElementById('lbl_usuario_logado');

    //Botões Atendimento
    var btn_incluir_ate = document.getElementById("btn_incluir_ate");
    var btn_alterar_ate = document.getElementById("btn_alterar_ate");
    var btn_remover_ate = document.getElementById("btn_remover_ate");

    //Controle de registros Existentes
    var id = "";

    //Definição de maskaras
    $('#txt_id_cto').mask('99.99.99.999A');
    $('#txt_rx_sinal').mask('-99.99');
    $('#txt_num_porta').mask('99');

    $('#txt_data_atendimento').mask('99/99/9999');
    $('#txt_ate_data').mask('99/99/9999');
    $('#txt_ate_hora').mask('99:99');
    $('#txt_ate_contato').mask('(99) 9 9999-9999');


    //Verifica Sessão ao iniciar
    SessionStatus();

    //keypress Function
    $.fn.enterKey = function (fnc) {
        return this.each(function () {
            $(this).keypress(function (ev) {
                var keycode = (ev.keyCode ? ev.keyCode : ev.which);
                if (keycode == '13') {
                    fnc.call(this, ev);
                }
            })
        })
    }

    //KeyPress
    $('#txt_id_cto').enterKey(function (e) {
        BuscaCto();
    });
    $('#txt_num_porta').enterKey(function () {
        BuscaCto();
    });
    $('#txt_cliente').enterKey(function () {
        BuscaCto();
    });
    $('#txt_rx_sinal').enterKey(function () {
        BuscaCto();
    });

    $('#txt_data_atendimento').enterKey(function () {
       BuscaAtendimento();
    });
    $('#txt_cliente_Atendimento').enterKey(function () {
        BuscaAtendimento();
    });
    $('#opt_assunto_atendimento').enterKey(function () {
        BuscaAtendimento();
    });
    $('#txt_status_atendimento').enterKey(function () {
        BuscaAtendimento();
    });

    //Botão Login Barra de Navegação
    $('#btn_login_nav').click(function () {
       if(logoff == false){
           CallDialogLogin();
       }else{
            SessionClose();
       }
    });

    $('#dialog-master').on('hide.bs.modal',function () {
        var modal = $(this);

        if(dialog == "login"){

            var user = modal.find('#txt_usuario_login').val();
            var pass = modal.find('#txt_senha_login').val();

            SessionStart(user,pass);
        }

        $('#dialog-body').html('');
    });

    $('#dialog-master').on('shown.bs.modal',function () {
        var modal = $(this);

    });

    $('#nav-home').click(function () {
        getHome();
    });

    $('#nav-cto').click(function () {
        if(!nav_cto.classList.contains('disabled')){
            getCto();
        }
    });

    $('#nav-atendimento').click(function () {
        if(!nav_atendimento.classList.contains('disabled')){
            getAtendimento();
        }
    });

    $('#btn_buscar_cto').click(function () {
        BuscaCto();
    });
    $('#btn_limpa_cto').click(function () {
        LimpaBuscaCto();
    });
    $('#btn_gravar_cto').click(function () {
       GravaCto();
    });

    $('#btn_busca_atendimento').click(function () {
        BuscaAtendimento();
    })
    $('#btn_incluir_ate').click(function () {
        CallIncluirAtendimento();
        id = "";
    });
    $('#btn_ate_cancelar').click(function () {
        CallBuscaAtendimento();
        id = "";
    });
    $('#btn_ate_salvar').click(function () {
        SalvaCadastroAtendimento();
        id = "";
    });
    $('#btn_alterar_ate').click(function () {
        CallAlterarAtendimento();
    });
    $('#btn_remover_ate').click(function () {
        RemoveAtendimento();
    });

    //Verifica Sessão
    function SessionStatus() {
        $.post(patch + 'session.php',{action:'session-status'})
            .done(function (data) {
                if(data == '0'){
                    CallDialogLogin();
                }else{
                    logoff = true;
                    LoginChanges(data);
                }
            })
            .fail(function() {
                alert( "Ocorreu um erro!" );
            });
    }

    function SessionStart(user,pass){
        $.post(patch + 'session.php',{action:'session-start', user:user, pass:pass})
            .done(function (data) {
                if(data != '0'){
                    logoff = true;
                    LoginChanges(data);
                }else{
                    nav_site.classList.add('disabled');
                    nav_cto.classList.add('disabled');
                    nav_atendimento.classList.add('disabled');
                }
            })
            .fail(function() {
                alert( "Ocorreu um erro!" );
            });
    }

    function SessionClose(){
        $.post(patch + 'session.php',{action:'session-close'})
            .done(function (data) {
                if(data == '1'){
                    logoff = false;
                    LoginChanges("");
                }
            })
            .fail(function() {
                alert( "Ocorreu um erro!" );
            });
    }


    // Chama dialog de Login
    function CallDialogLogin() {
        dialog = "login";
        $('#dialog-title').html("Login");
        $.get("screen/login.html",function (login) {
            $('#dialog-body').html(login);
        });
        $('#dialog-master').modal('show')
    }

    function CallIncluirAtendimento(){
        ate_busca.classList.add('d-none');
        ate_cadastro.classList.remove('d-none');
    }

    function CallBuscaAtendimento() {
        $('#txt_ate_solicitante').val('');
        $('#txt_ate_pppoe').val('');
        $('#txt_ate_data').val('');
        $('#txt_ate_hora').val('');
        $('#txt_ate_contato').val('');
        $('#txt_ate_protocolo').val('');
        $('#txt_ate_tratativas').val('');

        $('#opt_ate_assunto').selectedIndex = 0;
        $('#opt_ate_plataforma').selectedIndex = 0;
        $('#opt_ate_status').selectedIndex = 0;

        btn_alterar_ate.classList.add('disable');
        btn_remover_ate.classList.add('disable');

        ate_busca.classList.remove('d-none');
        ate_cadastro.classList.add('d-none');
    }

    function CallAlterarAtendimento() {
        id = selected_item.item(0).innerHTML;
        $('#txt_ate_solicitante').val(selected_item.item(1).innerHTML);
        $('#txt_ate_pppoe').val(selected_item.item(2).innerHTML);
        $('#txt_ate_data').val(selected_item.item(3).innerHTML);
        $('#txt_ate_hora').val(selected_item.item(4).innerHTML);
        $('#txt_ate_contato').val(selected_item.item(5).innerHTML);
        $('#opt_ate_plataforma').val(selected_item.item(6).innerHTML);
        $('#txt_ate_protocolo').val(selected_item.item(7).innerHTML);
        $('#opt_ate_assunto').val(selected_item.item(8).innerHTML);
        $('#txt_ate_tratativas').val(selected_item.item(9).innerHTML);
        $('#opt_ate_status').val(selected_item.item(10).innerHTML);

        CallIncluirAtendimento();
    }

    //Realiza Mudanças na Navbar conforme o login
    function LoginChanges(name){
        if(logoff == true){
            btn_login_nav.classList.remove('btn-primary');
            btn_login_nav.classList.add('btn-danger');
            $('#btn_login_nav').html("Sair");

            lbl_usuario_logado.classList.remove('invisible');
            $('#lbl_usuario_logado').html(name);

            nav_cto.classList.remove('disabled');
            nav_atendimento.classList.remove('disabled');
        }else{
            btn_login_nav.classList.add('btn-primary');
            btn_login_nav.classList.remove('btn-danger');
            $('#btn_login_nav').html("Login");

            lbl_usuario_logado.classList.add('invisible');
            $('#lbl_usuario_logado').html(name);

            nav_cto.classList.add('disabled');
            nav_atendimento.classList.add('disabled');
        }

        nav_home.classList.add('active');
        getHome();

    }

    function getHome() {
        nav_home.classList.add('active');
        nav_cto.classList.remove('active');
        nav_atendimento.classList.remove('active');

        form_home.classList.remove('d-none');
        form_cto.classList.add('d-none');
        form_atendimneto.classList.add('d-none');
    }

    function getCto(){
        nav_cto.classList.add('active');
        nav_home.classList.remove('active');
        nav_atendimento.classList.remove('active');

        form_home.classList.add('d-none');
        form_cto.classList.remove('d-none');
        form_atendimneto.classList.add('d-none');

        clearTimeout(interval_ocupation);
    }

    function getAtendimento() {
        nav_cto.classList.remove('active');
        nav_home.classList.remove('active');
        nav_atendimento.classList.add('active');

        form_home.classList.add('d-none');
        form_cto.classList.add('d-none');
        form_atendimneto.classList.remove('d-none');
    }


    function AlteraStatusButtonAtendimento() {
        if (id != ""){
            btn_alterar_ate.classList.remove('disabled');
            btn_remover_ate.classList.remove('disabled');
        }else{
            btn_alterar_ate.classList.add('disabled');
            btn_remover_ate.classList.add('disabled');
        }
    }

    function BuscaAtendimento() {
        var data = $('#txt_data_atendimento').val();
        var cliente = $('#txt_cliente_Atendimento').val();
        var assunto = $('#opt_assunto_atendimento').val();
        var status = $('#txt_status_atendimento').val();

        $.post(patch + 'function.php',{action:'busca-ate',data:data,cliente:cliente,assunto:assunto,status:status})
            .done(function (data) {

                corta = data.split("|||");

                $('#table_body_atendimento').html(corta[0]);
                $('#ate-listados').html('Listados ' + corta[1]);

                id = "";
                AlteraStatusButtonAtendimento();

                $('#table_body_atendimento tr').on('dblclick',function () {
                    try{
                        selected_item = this.children;

                        CallAlterarAtendimento();
                    }catch (e) {
                        alert(e)
                    }
                });

                $('#table_body_atendimento tr').on('click',function () {
                    $(this).addClass('bg-info').siblings().removeClass('bg-info');

                    selected_item = this.children;

                    id = selected_item.item(0).innerHTML;

                    AlteraStatusButtonAtendimento();
                });
            });
    }

    function SalvaCadastroAtendimento(){
        var solicitante = $('#txt_ate_solicitante').val().trim();
        var pppoe = $('#txt_ate_pppoe').val().trim();
        var data = $('#txt_ate_data').val().trim();
        var hora = $('#txt_ate_hora').val().trim();
        var contato = $('#txt_ate_contato').val().trim();
        var plataforma = $('#opt_ate_plataforma').val().trim();
        var protocolo = $('#txt_ate_protocolo').val().trim();
        var assunto = $('#opt_ate_assunto').val().trim();
        var status = $('#opt_ate_status').val().trim();
        var tratativas = $('#txt_ate_tratativas').val().trim();
        var usuarioCadastro = $('#lbl_usuario_logado').html();
        var horaCadastro = new Date().toISOString().slice(0, 19).replace('T', ' ');

        $.post(patch + 'function.php',{action:'grava-ate'
            ,id:id
            ,solicitante:solicitante
            ,pppoe:pppoe
            ,data:data
            ,hora:hora
            ,contato:contato
            ,plataforma:plataforma
            ,protocolo:protocolo
            ,assunto:assunto
            ,status:status
            ,tratativas:tratativas
            ,usuarioCadastro:usuarioCadastro
            ,horaCadastro:horaCadastro})
            .done(function (data) {
                $('#table_body_atendimento').html(data);

                if(data != ""){
                    alert('Cadastrado com Sucesso!');
                }

                id = "";
                AlteraStatusButtonAtendimento();

                CallBuscaAtendimento();

                $('#table_body_atendimento tr').on('dblclick',function () {
                    try{

                        selected_item = this.children;

                        CallAlterarAtendimento();
                    }catch (e) {
                        alert(e)
                    }
                });

                $('#table_body_atendimento tr').on('click',function () {
                    $(this).addClass('bg-info').siblings().removeClass('bg-info');

                    selected_item = this.children;

                    id = selected_item.item(0).innerHTML;

                    AlteraStatusButtonAtendimento();
                });

            });
    }

    function RemoveAtendimento() {

        $.post(patch + 'function.php',{action:'remove-ate',id:id})
            .done(function (data) {
                $('#table_body_atendimento').html(data);

                if(data != ""){
                    alert(data);
                }
                id = "";
                AlteraStatusButtonAtendimento();

                $('#table_body_atendimento tr').on('dblclick',function () {
                    try{

                        selected_item = this.children;

                        CallAlterarAtendimento();
                    }catch (e) {
                        alert(e)
                    }
                });

                $('#table_body_atendimento tr').on('click',function () {
                    $(this).addClass('bg-info').siblings().removeClass('bg-info');

                    selected_item = this.children;

                    id = selected_item.item(0).innerHTML;

                    AlteraStatusButtonAtendimento();
                });

            });

    }

    function BuscaCto() {
        var cto = $('#txt_id_cto').val().trim();
        var porta = $('#txt_num_porta').val().trim();
        var cliente = $('#txt_cliente').val().trim();
        var rx = $('#txt_rx_sinal').val().replace(",",".").trim();

        if (rx == '-00.00'){
            rx = '00.00';
        }

        $.post(patch + 'function.php',{action:'busca-cto',cto:cto,cliente:cliente,rx:rx})
            .done(function (data) {

                corta = data.split("|||");

                $('#table_body_cto').html(corta[0]);
                $('#cto-listados').html('Listados ' + corta[1])

                $('#table_body_cto tr').on('dblclick',function () {
                    try{

                        selected_item = this.children;

                        $('#txt_id_cto').val(selected_item.item(0).innerHTML);
                        $('#txt_num_porta').val(selected_item.item(1).innerHTML);
                        $('#txt_cliente').val(selected_item.item(2).innerHTML);
                        $('#txt_rx_sinal').val(selected_item.item(3).innerHTML);

                    }catch (e) {
                        alert(e)
                    }
                });

                $('#table_body_cto tr').on('click',function () {
                    $(this).addClass('bg-info').siblings().removeClass('bg-info');
                });
            })
            .fail(function () {
                alert( "Ocorreu um erro!" );
            });
    }

    function LimpaBuscaCto(){
        $('#txt_num_porta').val("");
        $('#txt_cliente').val("");
        $('#txt_rx_sinal').val("");
        $('#txt_id_cto').val("").focus();
    }

    function GravaCto(){
        var cto = $('#txt_id_cto').val().trim();
        var pt = $('#txt_num_porta').val().trim();
        var cliente = $('#txt_cliente').val().trim();
        var rx = $('#txt_rx_sinal').val().replace(",",".");
        var sufix = $('#cmb_sufix_cto').val();


        var cto_split = cto.split('.');

        if(cto_split[0].length == 2 && cto_split[1].length == 2)


        if (rx == '-00.00'){
            rx = '00.00';
        }

        if(sufix != ''){
            corta = cliente.split('@');

            if (corta.length > 1){
                cliente = corta[0];
            }

            if (cliente != '-' ){
                cliente = cliente + sufix;
            }
        }


        if (cto != "" && pt != "" && cliente != "" && rx != ""){
            $.post(patch + 'function.php',{action:'grava-cto',cto:cto,pt:pt,cliente:cliente,rx:rx})
                .done(function (data) {

                    $('#table_body_cto').html(data);

                    $('#txt_num_porta').val("").focus();
                    $('#txt_cliente').val("");
                    $('#txt_rx_sinal').val("");

                    $('#table_body_cto tr').on('dblclick',function () {
                        try{

                            selected_item = this.children;

                            $('#txt_id_cto').val(selected_item.item(0).innerHTML);
                            $('#txt_num_porta').val(selected_item.item(1).innerHTML);
                            $('#txt_cliente').val(selected_item.item(2).innerHTML);
                            $('#txt_rx_sinal').val(selected_item.item(3).innerHTML);

                        }catch (e) {
                            alert(e)
                        }
                    });

                })
                .fail(function () {
                    alert( "Ocorreu um erro!" );
                });
        }else{
            alert("Todos os Campos Devem ser preenchidos")
        }
    }

} );