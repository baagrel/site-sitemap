$(document).ready(function() {
    var patch = 'lib/php/';
    // Variável que indica o comando de logoff
    var logoff = false;

    //Variavel de controle de dialog
    var dialog ;

    //Guarda valor selecionado nas listas
    var selected_item;

    //Botões da barrade navegação lateral
    var nav_operacional = document.getElementById("nav-operacional");
    var nav_cliente = document.getElementById("nav-cliente");
    var nav_olt = document.getElementById("nav-olt");

    //Forms
    var form_operacional = document.getElementById("form-operacional");
    var form_cliente = document.getElementById("form-cliente");
    var form_olt = document.getElementById("form-olt");


    //Controle do Login
    var btn_login_nav = document.getElementById("btn_login_nav");
    var lbl_usuario_logado = document.getElementById('lbl_usuario_logado');


    //Definição de maskaras



    //############################################## CONTROLE SIDEBAR ######################

    $('#nav-operacional').click(function () {
        if(!nav_operacional.classList.contains('disabled')){
            getOperacional();
        }
    });

    $('#nav-cliente').click(function () {
        if(!nav_cliente.classList.contains('disabled')){
            getCliente();
        }
    });

    $('#nav-olt').click(function () {
        if(!nav_olt.classList.contains('disabled')){
            getOlt();
        }
    });

    function getOperacional(){
        nav_operacional.classList.add('active');
        nav_cliente.classList.remove('active');
        nav_olt.classList.remove('active');

        form_operacional.classList.remove('d-none');
        form_cliente.classList.add('d-none');
        form_olt.classList.add('d-none');
    }

    function getCliente(){
        nav_operacional.classList.remove('active');
        nav_cliente.classList.add('active');
        nav_olt.classList.remove('active');

        form_operacional.classList.add('d-none');
        form_cliente.classList.remove('d-none');
        form_olt.classList.add('d-none');
    }

    function getOlt(){
        nav_operacional.classList.remove('active');
        nav_cliente.classList.remove('active');
        nav_olt.classList.add('active');

        form_operacional.classList.add('d-none');
        form_cliente.classList.add('d-none');
        form_olt.classList.remove('d-none');
    }

    //#########################################################################################
    //######################### KEY PRESS #####################################################

    //keypress Function
    $.fn.enterKey = function (fnc) {
        return this.each(function () {
            $(this).keypress(function (ev) {
                var keycode = (ev.keyCode ? ev.keyCode : ev.which);
                if (keycode == '13') {
                    fnc.call(this, ev);
                }
            })
        })
    };


    //########################################################################################
    //#################################### CONTROLE DE SESSÃO ###############################
    SessionStatus();

    //Botão Login Barra de Navegação
    $('#btn_login_nav').click(function () {
        if(logoff == false){
            CallDialogLogin();
        }else{
            SessionClose();
        }
    });

    //Verifica Sessão
    function SessionStatus() {
        $.post(patch + 'session.php',{action:'session-status'})
            .done(function (data) {
                if(data == '0'){
                    CallDialogLogin();
                }else{
                    logoff = true;
                    LoginChanges(data);
                }
            })
            .fail(function() {
                alert( "Ocorreu um erro!" );
            });
    }

    function SessionStart(user,pass){
        $.post(patch + 'session.php',{action:'session-start', user:user, pass:pass})
            .done(function (data) {
                if(data != '0'){
                    logoff = true;
                    LoginChanges(data);
                }else{
                    nav_operacional.classList.add('disabled');
                    nav_cliente.classList.add('disabled');
                    nav_olt.classList.add('disabled');
                }
            })
            .fail(function() {
                alert( "Ocorreu um erro!" );
            });
    }

    function SessionClose(){
        $.post(patch + 'session.php',{action:'session-close'})
            .done(function (data) {
                if(data == '1'){
                    logoff = false;
                    LoginChanges("");
                }
            })
            .fail(function() {
                alert( "Ocorreu um erro!" );
            });
    }

    //Realiza Mudanças na Navbar conforme o login
    function LoginChanges(name){
        if(logoff == true){
            btn_login_nav.classList.remove('btn-primary');
            btn_login_nav.classList.add('btn-danger');
            $('#btn_login_nav').html("Sair");

            lbl_usuario_logado.classList.remove('invisible');
            $('#lbl_usuario_logado').html(name);

            nav_operacional.classList.remove('disabled');
            nav_cliente.classList.remove('disabled');
            nav_olt.classList.remove('disabled');

            getOperacional();
        }else{
            btn_login_nav.classList.add('btn-primary');
            btn_login_nav.classList.remove('btn-danger');
            $('#btn_login_nav').html("Login");

            lbl_usuario_logado.classList.add('invisible');
            $('#lbl_usuario_logado').html(name);

            nav_operacional.classList.add('disabled');
            nav_cliente.classList.add('disabled');
            nav_olt.classList.add('disabled');
        }

    }
    //##########################################################################
    //############################### DIALOG LOGIN #############################

    $('#dialog-master').on('hide.bs.modal',function () {
        var modal = $(this);

        if(dialog == "login"){

            var user = modal.find('#txt_usuario_login').val();
            var pass = modal.find('#txt_senha_login').val();

            SessionStart(user,pass);
        }

        $('#dialog-body').html('');
    });

    $('#dialog-master').on('shown.bs.modal',function () {
        var modal = $(this);

    });

    // Chama dialog de Login
    function CallDialogLogin() {
        dialog = "login";
        $('#dialog-title').html("Login");
        $.get("screen/login.html",function (login) {
            $('#dialog-body').html(login);
        });
        $('#dialog-master').modal('show')
    }

    //##########################################################################



} );