$(document).ready(function() {
    var patch = '../lib/php/';

    BuscaClientesPon();
    setInterval(BuscaClientesPon,100000);

    function BuscaClientesPon(){
        $.post(patch + 'function.php',{action:'busca-ocupacao'})
            .done(function (data) {
                $('#table_body_ocupacao').html(data);
            })
            .fail(function () {
                alert( "Ocorreu um erro!" );
            });
    }
});