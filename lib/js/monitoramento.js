$(document).ready(function() {
    var olt = 0;
    var slot = 0;

    
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = null;


    Refresh();
    setInterval(Refresh,15000);

    function addData(chart, label, data){

        if(olt = '0'){
            chart.data.labels.push(label);
            value = '';
            chart.data.datasets.forEach((dataset) => {
                data.forEach((lines)=>{
                    if(dataset.ramal == lines.ramal){
                        dataset.backgroundColor.push(paleta_de_cores(dataset.id));
                        dataset.data.push(lines.online);

                        value = value + '<tr style="background-color:' + paleta_de_cores(dataset.id) +
                            '"><td><b>RB ' + lines.ramal + ' : ' + lines.online + '</b></td></tr>'
                    }
                });
            });
            $('#table_body_monitor').html(value);
            chart.update();
        }else{

        }
    }

    function removeData(chart) {
        if(olt = '0'){
            if (chart.data.labels.length > 60){
                chart.data.labels.shift();
                chart.data.datasets.forEach((dataset) => {
                    dataset.data.shift();
                });
                chart.update();
            }
        }
    }

    function ConverteDadosInicial(result) {
        var arr = JSON.parse(result);
        var dataset = [];
        var cont = 0;

        if(olt == 0){
            arr.forEach((obj)=>{
                cont = cont + 1;
                var cor = paleta_de_cores(cont);
                 dataset.push(
                 {
                     id: cont,
                     ramal: obj.ramal,
                     label: 'Ramal ' + obj.ramal,
                     data: [obj.online],
                     fill: false,
                     borderWidth: 3,
                     backgroundColor: [
                         cor
                     ],
                     borderColor: [
                         cor
                     ]
                 })
            });
        }

        return dataset;
    }

    function CriaChart(dataset) {

        if(olt == '0'){
            var value = ConverteDadosInicial(dataset);
            myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: [],
                    datasets: value
                },options: {
                    showAllTooltips: true,
                    responsive: true,
                    scales: {
                        yAxes: [{
                            ticks: {
                                suggestedMin: 0,
                                suggestedMax: 50
                            },
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Clientes Online'
                            }
                        }],
                        xAxes: [{
                            ticks: {
                                maxTicksLimit: 15
                            },
                            display: true
                        }]
                    },
                    tooltips: {
                        mode: 'nearest',
                        intersect: false
                    },
                    hover: {
                        display: true,
                        mode: 'nearest',
                        intersect: true
                    },
                    legend: {
                        labels: {
                            fontColor: 'black',
                            fontSize: 30
                        }
                    }
                }

            });
        }
    }

    var options = {
        tooltipTemplate: "<%= value %>",

        showTooltips: true,
        tooltipEvents: []
    }
    
    function Refresh() {
        $.post('../lib/php/monitoramento_olt.php',{action:'monitoramento_v2', olt:olt, slot:slot})
            .done(function (data) {
                if(myChart == null){
                    CriaChart(data);
                    busca_historico();
                }else{
                    var date = new Date();

                    var hora = date.getHours() + ':' + date.getMinutes();

                    var decode = JSON.parse(data);

                    addData(myChart,hora,decode);
                    removeData(myChart);
                }
            })
            .fail(function() {
            });
    }

    function busca_historico() {
        $.post('../lib/php/monitoramento_olt.php',{action:'busca_historico'})
            .done(function (data) {
                var decode = JSON.parse(data);
                var cont = 0;
                decode.forEach((obj)=>{
                    if (cont > 4){
                        addData(myChart,obj.hora,obj.online);
                        removeData(myChart);
                        cont = 0;
                    }else{
                        cont = cont + 1
                    }

                });
            }).fail(function() {

            });
    }

    function paleta_de_cores(src) {
        var retorno = '';
        switch (src) {
            case 1:
                retorno = 'rgb(11,255,24)';
                break;
            case 2:
                retorno = 'rgb(4,171,255)';
                break;
            case 3:
                retorno = 'rgb(255,106,0)';
                break;
            case 4:
                retorno = 'rgb(255,37,5)';
                break;
            case 5:
                retorno = 'rgb(255,4,131)';
                break;
            case 6:
                retorno = 'rgb(250,10,255)';
                break;
            case 7:
                retorno = 'rgb(123,0,255)';
                break;
            case 8:
                retorno = 'rgb(254,255,8)';
                break;
            case 9:
                retorno = 'rgb(164,255,140)';
                break;
            case 10:
                retorno = 'rgb(250,255,120)';
                break;
            case 11:
                retorno = 'rgb(255,186,117)';
                break;
            case 12:
                retorno = 'rgb(255,128,123)';
                break;
            case 13:
                retorno = 'rgb(255,149,230)';
                break;
            case 14:
                retorno = 'rgb(187,138,255)';
                break;
            case 15:
                retorno = 'rgb(116,127,255)';
                break;
            case 16:
                retorno = 'rgb(114,247,255)';
                break;
        }
        return retorno;
    }
});