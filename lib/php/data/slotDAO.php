<?php

require_once 'connection.php';
class slotDAO
{
    private $db;

    public function __construct(){
        try{
            $this->db = new DB_CONNECT();
        }catch (Exception $ex){
            echo $ex;
        }
    }

    function BuscaSlots($ramal){
        $conn = $this->db->getConnection();

        $busca = "Select * from `Slot` where `ramal` = ?";

        $stm = new PDOStatement();
        $stm = $conn->prepare($busca);
        $stm->bindValue(1, $ramal);
        $stm->execute();

        return json_encode($stm->fetchAll(PDO::FETCH_OBJ));
    }
}