<?php

require_once 'ramal.php';
require_once 'connection.php';

class RamalDAO
{
    private $db;

    public function __construct(){
        try{
            $this->db = new DB_CONNECT();
        }catch (Exception $ex){
            echo $ex;
        }
    }

    function RamalList(){
        $conn = $this->db->getConnection();

        $query = "Select * from `Ramal`";

        $stm = new PDOStatement();
        $stm = $conn->prepare($query);
        $stm->execute();

        return json_encode($stm->fetchAll(PDO::FETCH_OBJ));
    }
}