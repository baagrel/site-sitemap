<?php

require_once 'database.php';
class DB_CONNECT {

    private $conn;

    function __construct() {
        try {
            $this->conn = new PDO("mysql:host=" . host . "; dbname=" . dbname . "", user, pass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        } catch (Exception $ex) {
            $this->conn = null;
        }
    }

    function getConnection() {
        return $this->conn;
    }
}

