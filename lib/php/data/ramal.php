<?php


class ramal
{
    private $id;
    private $nomeRamal;
    private $ipRamal;
    private $numRamal;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNomeRamal()
    {
        return $this->nomeRamal;
    }

    /**
     * @param mixed $nomeRamal
     */
    public function setNomeRamal($nomeRamal)
    {
        $this->nomeRamal = $nomeRamal;
    }

    /**
     * @return mixed
     */
    public function getIpRamal()
    {
        return $this->ipRamal;
    }

    /**
     * @param mixed $ipRamal
     */
    public function setIpRamal($ipRamal)
    {
        $this->ipRamal = $ipRamal;
    }

    /**
     * @return mixed
     */
    public function getNumRamal()
    {
        return $this->numRamal;
    }

    /**
     * @param mixed $numRamal
     */
    public function setNumRamal($numRamal)
    {
        $this->numRamal = $numRamal;
    }


}