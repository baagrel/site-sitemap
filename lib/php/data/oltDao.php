<?php

require_once 'connection.php';
require_once 'olt.php';
class oltDao
{
    private $db;

    public function __construct(){
        try{
            $this->db = new DB_CONNECT();
        }catch (Exception $ex){
            echo $ex;
        }
    }

    function monitoramento($ramal, $slot){
        try{
            $result = '[';

            if($ramal == '0'){
                $conn = $this->db->getConnection();

                $stm = new PDOStatement();

                $busca = "Select * from `Ramal` ";
                $stm = $conn->prepare($busca);
                $stm->execute();

                while($linha = $stm->fetch(PDO::FETCH_OBJ)){
                    $ramal = $linha->numRamal;

                    $busca2 = "Select * from `olt` where `ramal` = ?";

                    $stm1 = $conn->prepare($busca2);
                    $stm1->bindValue(1,$ramal);
                    $stm1->execute();

                    $online = 0;
                    while ($linha2 = $stm1->fetch(PDO::FETCH_OBJ)){
                        if($linha2->status == 'up'){
                            $online = $online + 1;
                        }
                    }
                    $result = $result . '{"ramal":"' . $ramal . '", "online":"' . $online . '"},';
                }
            }else{

            }

            $result = substr($result,0,strlen($result) -1) . ']';

            return $result;
        }catch (Exception $ex){
            echo $ex;
        }
    }


    function decode_and_save($data){
        try{
            $conn = $this->db->getConnection();
            $stm = new PDOStatement();
            $stm1 = new PDOStatement();

            $jsonData = json_decode($data);
            $return = '';
            foreach ($jsonData as $arr1){
                foreach ($arr1 as $arr2){
                    if(is_array($arr2)){
                        $pon = 0;
                        $slot = 0;
                        $ramal = 0;
                        $online = array();
                        foreach ($arr2 as $arr3){
                            $ramal = intval($arr3->ramal);
                            $pon = intval($arr3->pon);
                            $slot = intval($arr3->slot);
                            $onu_id = intval($arr3->onu_id);
                            $fhtt = $arr3->fhtt;
                            $model = $arr3->model;
                            $status = $arr3->status;

                            $busca = "Select `id` from `olt` where `fhtt` = ?";

                            $stm = $conn->prepare($busca);
                            $stm->bindValue(1,$fhtt);
                            $stm->execute();

                            $id = 0;
                            while($linha = $stm->fetch(PDO::FETCH_OBJ)){
                                $id = $linha->id;
                            }

                            if($id != 0){
                                $grava = "Update `olt` set `ramal` = ?, `slot` = ?, `pon` = ?, `onu_id` = ?, `model` = ?, `status` = ? where (`id` = ?)";

                                $stm1 = $conn->prepare($grava);
                                $stm1->bindValue(1,$ramal);
                                $stm1->bindValue(2,$slot);
                                $stm1->bindValue(3,$pon);
                                $stm1->bindValue(4,$onu_id);
                                $stm1->bindValue(5,$model);
                                $stm1->bindValue(6,$status);
                                $stm1->bindValue(7,$id);
                                $stm1->execute();
                            }else{
                                $grava = "Insert into `olt` (`ramal`,`slot`,`pon`,`onu_id`,`fhtt`,`model`,`status`) values (?,?,?,?,?,?,?)";

                                $stm1 = $conn->prepare($grava);
                                $stm1->bindValue(1,$ramal);
                                $stm1->bindValue(2,$slot);
                                $stm1->bindValue(3,$pon);
                                $stm1->bindValue(4,$onu_id);
                                $stm1->bindValue(5,$fhtt);
                                $stm1->bindValue(6,$model);
                                $stm1->bindValue(7,$status);
                                $stm1->execute();

                                $id = $conn->lastInsertId();
                            }
                            array_push($online,$id);

                        }

                        $busca = "Select `id` from `olt` where `ramal` = ? and `slot` = ? and `pon` = ?";
                        $stm = $conn->prepare($busca);
                        $stm->bindValue(1,$ramal);
                        $stm->bindValue(2,$slot);
                        $stm->bindValue(3,$pon);
                        $stm->execute();

                        while($linha = $stm->fetch(PDO::FETCH_OBJ)){
                            if (!in_array($linha->id,$online)){
                                $grava = 'Delete from `olt` where `id` = ?';

                                $stm1 = $conn->prepare($grava);
                                $stm1->bindValue(1,$linha->id);
                                $stm1->execute();
                            }
                        }

                    }else{
                        $pon = $arr2->pon;
                        $slot = $arr2->slot;
                        $ramal = $arr2->ramal;

                        $busca = "Select `id` From `olt` where `ramal` = ? and `slot` = ? and `pon` = ? ";

                        $stm = $conn->prepare($busca);
                        $stm->bindValue(1,$ramal);
                        $stm->bindValue(2,$slot);
                        $stm->bindValue(3,$pon);
                        $stm->execute();

                        while($linha = $stm->fetch(PDO::FETCH_OBJ)){
                            $grava = 'Delete from `olt` where `id` = ?';

                            $stm1 = $conn->prepare($grava);
                            $stm1->bindValue(1, $linha->id);
                            $stm1->execute();
                        }
                    }
                }
            }

            $this->salva_historico();

            return 'Ok...';
        }catch (Exception $e){
            return 'Error...';
        }
    }

    function salva_historico(){
        $conn = $this->db->getConnection();
        $stm = new PDOStatement();

        $busca = "Select `hora` from `historico_olt` order by id desc limit 0,1";

        $stm = $conn->prepare($busca);
        $stm->execute();

        $hora = '';
        while($linha = $stm->fetch(PDO::FETCH_OBJ)){
            $hora = $linha->hora;
        }

        if($hora != ''){

            $hora = new DateTime($hora);
            $diff = $hora->diff( new DateTime(date('Y-m-d H:i:s')) );

            if ($diff->format('%i') >= 2){
                $hora_atual = date('Y-m-d H:i:s');
                $historico = $this->monitoramento(0,0);

                $grava = "Insert into `historico_olt` (`hora`,`online`) values (?,?)";

                $stm = $conn->prepare($grava);
                $stm->bindValue(1,$hora_atual);
                $stm->bindValue(2,$historico);
                $stm->execute();

                echo 'historico...';
            }

        }else{
            $hora_atual = date('Y-m-d H:i:s');
            $historico = $this->monitoramento(0,0);

            $grava = "Insert into `historico_olt` (`hora`,`online`) values (?,?)";

            $stm = $conn->prepare($grava);
            $stm->bindValue(1,$hora_atual);
            $stm->bindValue(2,$historico);
            $stm->execute();
        }

    }


    function LimpaHistorico(){
        $conn = $this->db->getConnection();
        $stm = new PDOStatement();

        $hora_atual = date('Y-m-d H:i:s');
        $busca = "Select * from `historico_olt` order by id";

        $stm = $conn->prepare($busca);
        $stm->execute();

        while($linha = $stm->fetch(PDO::FETCH_OBJ)){
            $hora = new DateTime($linha->hora);
            $diff = $hora->diff(new DateTime($hora_atual));

            if($diff->format('%H') > 24){
                $remove = "Delete from `historico_olt` where `id` = ?";

                $stm1 = $conn->prepare($remove);
                $stm1->bindValue(1,$linha->id);
                $stm->execute();
            }

        }
    }

    function BuscaHIstorico(){
        $conn = $this->db->getConnection();
        $stm = new PDOStatement();
        $stm1 = new PDOStatement();

        $busca = "Select * from `historico_olt` order by id";

        $stm = $conn->prepare($busca);
        $stm->execute();

        $result = '[';
        while($linha = $stm->fetch(PDO::FETCH_OBJ)){
            $date_time = new DateTime($linha->hora);
            $hora = $date_time->format('H:i');
            $result = $result. '{"hora":"'.$hora.'","online":'.$linha->online.'},';
        }

        $result = substr($result,0,strlen($result) -1) . ']';

        return $result;
    }

    function BuscaOcupacao(){
        $conn = $this->db->getConnection();
        $stm = new PDOStatement();

        $busca = "Select * from `Slot` order by `ramal`";

        $stm = $conn->prepare($busca);
        $stm->execute();

        $return = '';
        while($linha = $stm->fetch(PDO::FETCH_OBJ)){
            $max = $linha->pon;

            $return = $return. '<tr><td class="bg-dark text-white"><b>RB '.$linha->ramal.' SL '.$linha->slot.'</b></td>';
            $cont = 1;
            while ($max >= $cont){
                $busca2 = "Select * from `olt` where `ramal` = ? and `slot` = ? and `pon` = ?";

                $stm1 = $conn->prepare($busca2);
                $stm1->bindValue(1,$linha->ramal);
                $stm1->bindValue(2,$linha->slot);
                $stm1->bindValue(3,$cont);
                $stm1->execute();

                $total = 0;
                while ($linha2 = $stm1->fetch(PDO::FETCH_OBJ)){
                    $total = $total + 1;
                }

                if($total >= 65 and $total < 85){
                    $return = $return.'<td class="bg-warning"><b>'.$total.'</b></td>';
                }else if($total >= 85){
                    $return = $return.'<td class="bg-danger"><b>'.$total.'</b></td>';
                }else{
                    $return = $return.'<td><b>'.$total.'</b></td>';
                }
                $cont = $cont +1;

            }
            $return = $return.'</tr>';
        }

        return $return;
    }

}