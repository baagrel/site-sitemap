<?php


class olt
{
    private $id;
    private $ramal;
    private $slot;
    private $pon;
    private $onu_id;
    private $fhtt;
    private $model;
    private $status;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getRamal()
    {
        return $this->ramal;
    }

    /**
     * @param mixed $ramal
     */
    public function setRamal($ramal)
    {
        $this->ramal = $ramal;
    }

    /**
     * @return mixed
     */
    public function getSlot()
    {
        return $this->slot;
    }

    /**
     * @param mixed $slot
     */
    public function setSlot($slot)
    {
        $this->slot = $slot;
    }

    /**
     * @return mixed
     */
    public function getPon()
    {
        return $this->pon;
    }

    /**
     * @param mixed $pon
     */
    public function setPon($pon)
    {
        $this->pon = $pon;
    }

    /**
     * @return mixed
     */
    public function getOnuId()
    {
        return $this->onu_id;
    }

    /**
     * @param mixed $onu_id
     */
    public function setOnuId($onu_id)
    {
        $this->onu_id = $onu_id;
    }

    /**
     * @return mixed
     */
    public function getFhtt()
    {
        return $this->fhtt;
    }

    /**
     * @param mixed $fhtt
     */
    public function setFhtt($fhtt)
    {
        $this->fhtt = $fhtt;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param mixed $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }


}