<?php

require_once 'connection.php';
require_once 'cliente.php';
require_once 'RamalDAO.php';

class clienteDao
{
    private $db;

    /**
     * usuarioDAO constructor.
     */
    public function __construct(){
        try{
            $this->db = new DB_CONNECT();
        }catch (Exception $ex){
            echo $ex;
        }
    }
    function BuscaPons($ramal){
        $conn = $this->db->getConnection();

        $query = "Select distinct `pon`,`slot`,`ramal` from `Cliente` where `ramal` = ?  order by `ramal`, `slot`, `pon` asc";

        $stm = new PDOStatement();
        $stm = $conn->prepare($query);
        $stm->bindValue(1,$ramal);
        $stm->execute();

        return json_encode($stm->fetchAll(PDO::FETCH_OBJ));
    }

    function DecodeAndSave($data){
        try{
            $conn = $this->db->getConnection();
            $stm = new PDOStatement();

            $jsonData = json_decode($data);
            $return = '';
            foreach ($jsonData as $arr1){
                foreach ($arr1 as $arr2){
                    if(is_array($arr2)){
                        $pon = 0;
                        $slot = 0;
                        $ramal = 0;
                        $online = array();
                        foreach ($arr2 as $arr3){
                            $ramal = intval($arr3->ramal);
                            $pon = intval($arr3->pon);
                            $slot = intval($arr3->slot);
                            $onuId = intval($arr3->onuId);
                            $fhtt = $arr3->fhtt;

                            $busca = "Select `id` from `Cliente` where `ramal` = ? and `slot` = ? and `pon` = ? and `onuId` = ?";

                            $stm = $conn->prepare($busca);
                            $stm->bindValue(1,$ramal);
                            $stm->bindValue(2,$slot);
                            $stm->bindValue(3,$pon);
                            $stm->bindValue(4,$onuId);
                            $stm->execute();


                            while($linha = $stm->fetch(PDO::FETCH_OBJ)){
                                $grava = "Update `Cliente` set `online` = '1', `fhtt` = ? where (`id` = ?)";

                                $stm1 = $conn->prepare($grava);
                                $stm1->bindValue(1,$fhtt);
                                $stm1->bindValue(2,$linha->id);
                                $stm1->execute();

                                array_push($online,$linha->id);
                            }

                        }

                        $busca = "Select `id` from `Cliente` where `ramal` = ? and `slot` = ? and `pon` = ?";
                        $stm = $conn->prepare($busca);
                        $stm->bindValue(1,$ramal);
                        $stm->bindValue(2,$slot);
                        $stm->bindValue(3,$pon);
                        $stm->execute();

                        while($linha = $stm->fetch(PDO::FETCH_OBJ)){
                            if (!in_array($linha->id,$online)){
                                $grava = 'Update `Cliente` set `online` = 0 where `id` = ?';

                                $stm1 = $conn->prepare($grava);
                                $stm1->bindValue(1,$linha->id);
                                $stm1->execute();
                            }
                        }

                    }else{
                        if($arr2->ramal != ''){
                            $pon = $arr2->pon;
                            $slot = $arr2->slot;
                            $ramal = $arr2->ramal;

                            $busca = "Select `id` From `Cliente` where `ramal` = ? and `slot` = ? and `pon` = ? ";

                            $stm = $conn->prepare($busca);
                            $stm->bindValue(1,$ramal);
                            $stm->bindValue(2,$slot);
                            $stm->bindValue(3,$pon);
                            $stm->execute();

                            while($linha = $stm->fetch(PDO::FETCH_OBJ)){
                                $grava = 'Update `Cliente` set `online` = 0 where `id` = ?';

                                $stm1 = $conn->prepare($grava);
                                $stm1->bindValue(1, $linha->id);
                                $stm1->execute();
                            }
                        }
                    }
                }
            }
            return 'Ok...';
        }catch (Exception $e){
            return 'Error...';
        }


    }

    function Busca($olt, $slot, $pon){
        $result = '[';
        if($olt == '0'){
            $conn = $this->db->getConnection();

            $stm = new PDOStatement();

            $busca = "Select * from `Ramal` ";
            $stm = $conn->prepare($busca);
            $stm->execute();

            while($linha = $stm->fetch(PDO::FETCH_OBJ)){
                $ramal = $linha->numRamal;

                $busca2 = "Select * from `Cliente` where `ramal` = ?";

                $stm1 = $conn->prepare($busca2);
                $stm1->bindValue(1,$ramal);
                $stm1->execute();

                $online = 0;
                while ($linha2 = $stm1->fetch(PDO::FETCH_OBJ)){
                    if($linha2->online == 1){
                        $online = $online + 1;
                    }
                }
                $result = $result . '{"ramal":"' . $ramal . '", "online":"' . $online . '"},';
            }
        }else{

        }

        $result = substr($result,0,strlen($result) -1) . ']';

        return $result;

    }
}