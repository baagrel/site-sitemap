<?php


class cliente
{
    private $id;
    private $pppoe;
    private $ramal;
    private $slot;
    private $pon;
    private $onuid;
    private $fhtt;
    private $ssid;
    private $pass;
    private $online;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPppoe()
    {
        return $this->pppoe;
    }

    /**
     * @param mixed $pppoe
     */
    public function setPppoe($pppoe)
    {
        $this->pppoe = $pppoe;
    }

    /**
     * @return mixed
     */
    public function getRamal()
    {
        return $this->ramal;
    }

    /**
     * @param mixed $ramal
     */
    public function setRamal($ramal)
    {
        $this->ramal = $ramal;
    }

    /**
     * @return mixed
     */
    public function getSlot()
    {
        return $this->slot;
    }

    /**
     * @param mixed $slot
     */
    public function setSlot($slot)
    {
        $this->slot = $slot;
    }

    /**
     * @return mixed
     */
    public function getPon()
    {
        return $this->pon;
    }

    /**
     * @param mixed $pon
     */
    public function setPon($pon)
    {
        $this->pon = $pon;
    }

    /**
     * @return mixed
     */
    public function getOnuid()
    {
        return $this->onuid;
    }

    /**
     * @param mixed $onuid
     */
    public function setOnuid($onuid)
    {
        $this->onuid = $onuid;
    }

    /**
     * @return mixed
     */
    public function getFhtt()
    {
        return $this->fhtt;
    }

    /**
     * @param mixed $fhtt
     */
    public function setFhtt($fhtt)
    {
        $this->fhtt = $fhtt;
    }

    /**
     * @return mixed
     */
    public function getSsid()
    {
        return $this->ssid;
    }

    /**
     * @param mixed $ssid
     */
    public function setSsid($ssid)
    {
        $this->ssid = $ssid;
    }

    /**
     * @return mixed
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @param mixed $pass
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
    }

    /**
     * @return mixed
     */
    public function getOnline()
    {
        return $this->online;
    }

    /**
     * @param mixed $online
     */
    public function setOnline($online)
    {
        $this->online = $online;
    }



}