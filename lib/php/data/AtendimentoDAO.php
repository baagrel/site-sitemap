<?php

require_once 'connection.php';

class AtendimentoDAO
{

    private $db;


    public function __construct(){
        try{
            $this->db = new DB_CONNECT();
        }catch (Exception $ex){
            echo $ex;
        }
    }

    function RemoveAtendimento($id){
        if($id != ""){
            $query = "Select * from `Atendimento` where `id` = ?";

            $conn = $this->db->getConnection();
            $stm = $conn->prepare($query);
            $stm->bindValue(1,$id);
            $stm->execute();

            $existente = false;
            $pppoe = "";
            while($linha = $stm->fetch(PDO::FETCH_OBJ)){
                $existente = true;
                $pppoe = $linha->pppoe;
                break;
            }

            if($existente == true){
                $remove = "Delete from `Atendimento` where `id` = ?";

                $stm = $conn->prepare($remove);
                $stm->bindValue(1,$id);
                $stm->execute();

                return 'Registro id '.$id.' de atendimento a '.$pppoe.' removido com Sucesso!';
            }else {
                return 'Registro a ser Removido não foi Encontrado!';
            }
        }else{
            return 'Registro a ser Removido não foi Encontrado!';
        }
    }

    function BuscaAtendimento($pppoe,$data,$assunto,$status){

        if(stripos($data, "/")){
            $date = new DateTime();
            $strDate = explode("/",$data);
            $date->setDate($strDate[2],$strDate[1],$strDate[0]);
        }

        $conn = $this->db->getConnection();

        $query = "SELECT * FROM `Atendimento` WHERE";

        if($pppoe != ""){
            $pppoe = '%'.$pppoe.'%';
            $query = $query . " `pppoe` LIKE ? AND";
        }
        if($data != ""){
            $query = $query . " `data` = ? AND";
        }

        if($assunto != ""){
            $query = $query . " `assunto` = ? AND";
        }

        if($status != ""){
            $query = $query . " `status` = ?";
        }


        if(substr($query,-5) == "WHERE"){
            $query = substr($query,0,strlen($query) -5);
        }
        if(substr($query, -3) == "AND"){
            $query =  substr($query, 0,strlen($query) -3);
        }

        $query = $query . "ORDER by `data` DESC, `hora` DESC";

        $stm = new PDOStatement();
        $stm = $conn->prepare($query);
        $c = 0;

        if($pppoe != ""){
            $c = $c + 1;
            $stm->bindValue($c,$pppoe);
        }
        if($data != ""){
            $c = $c + 1;
            $stm->bindValue($c,$date->format("y-m-d"));
        }

        if($assunto != ""){
            $c = $c + 1;
            $stm->bindValue($c,$assunto);
        }

        if($status != ""){
            $c = $c + 1;
            $stm->bindValue($c,$status);
        }

        $stm->execute();
        $str = "";
        $itens = 0;

        while($linha = $stm->fetch(PDO::FETCH_OBJ)){
            $status = $linha->status;
            $date = new DateTime($linha->data);
            $showDate = $date->format('d/m/Y');
            $showHora = substr($linha->hora,0,5);

            if($status == 'Aberto'){
                $str = $str."<tr class='bg-warning'>
                <td class='d-none'>$linha->id</td>
                <td>$linha->solicitante</td>
                <td>$linha->pppoe</td>
                <td>$showDate</td>
                <td>$showHora</td>
                <td class='d-none'>$linha->contato</td>
                <td class='d-none'>$linha->plataforma</td>
                <td class='d-none'>$linha->protocolo</td>
                <td>$linha->assunto</td>
                <td class='d-none'>$linha->tratativas</td>
                <td>$linha->status</td>
                <td class='d-none'>$linha->usuarioCadastro</td>
                <td class='d-none'>$linha->horaCadastro</td> 
                </tr>";
            }else{
                $str = $str."<tr>
                <td class='d-none'>$linha->id</td>
                <td>$linha->solicitante</td>
                <td>$linha->pppoe</td>
                <td>$showDate</td>
                <td>$showHora</td>
                <td class='d-none'>$linha->contato</td>
                <td class='d-none'>$linha->plataforma</td>
                <td class='d-none'>$linha->protocolo</td>
                <td>$linha->assunto</td>
                <td class='d-none'>$linha->tratativas</td>
                <td>$linha->status</td>
                <td class='d-none'>$linha->usuarioCadastro</td>
                <td class='d-none'>$linha->horaCadastro</td>
                </tr>";
            }
            $itens = $itens + 1;

        }
        return $str.'|||'.$itens;



    }

    function GravaAtendimento($id,$solicitante,$pppoe,$data,$hora,$contato,$plataforma,$protocolo,$assunto,
                              $status,$tratativas,$usuarioCadastro, $horaCadastro){
        $conn = $this->db->getConnection();

        $data = explode("/",$data);
        $date = new DateTime();
        $date->setDate($data[2],$data[1],$data[0]);


        if($id == ''){
            $grava = "Insert into `Atendimento` (`solicitante`, `pppoe`, `data`, `hora`, `contato`, `plataforma`,
            `protocolo`, `assunto`, `tratativas`, `status`, `usuarioCadastro`, `horaCadastro`) values (?,?,?,?,?,?,?,?,?,?,?,?)";
        }else{
            $grava = "Update `Atendimento` set `solicitante` = ?, `pppoe` = ?,`data` = ?,`hora`= ?,`contato`= ?,
            `plataforma`= ?,`protocolo`= ?,`assunto`= ?,`tratativas`= ?,`status`= ?,`usuarioCadastro`= ?,
            `horaCadastro`= ? where `id` = ?";
        }

        try{
            $stm = $conn->prepare($grava);
            $stm->bindValue(1,$solicitante);
            $stm->bindValue(2,$pppoe);
            $stm->bindValue(3,$date->format('y-m-d'));
            $stm->bindValue(4,$hora);
            $stm->bindValue(5,$contato);
            $stm->bindValue(6,$plataforma);
            $stm->bindValue(7,$protocolo);
            $stm->bindValue(8,$assunto);
            $stm->bindValue(9,$tratativas);
            $stm->bindValue(10,$status);
            $stm->bindValue(11,$usuarioCadastro);
            $stm->bindValue(12,$horaCadastro);

            if($id != ""){
                $stm->bindValue(13,$id);
            }

            $stm->execute();

            return $this->BuscaAtendimento($pppoe,$date->format('d/m/y')
                ,$assunto,$status);
        }catch (PDOException $e){
            return $e->getCode();
        }

    }


}