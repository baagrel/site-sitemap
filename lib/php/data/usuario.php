<?php
class usuario{
    private $id;
    private $nomeUsuario;
    private $senhaUsuario;
    private $nomeCompletoUsuario;
    private $telefoneUsuario;
    private $cpfUsuario;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNomeUsuario()
    {
        return $this->nomeUsuario;
    }

    /**
     * @param mixed $nomeUsuario
     */
    public function setNomeUsuario($nomeUsuario)
    {
        $this->nomeUsuario = $nomeUsuario;
    }

    /**
     * @return mixed
     */
    public function getSenhaUsuario()
    {
        return $this->senhaUsuario;
    }

    /**
     * @param mixed $senhaUsuario
     */
    public function setSenhaUsuario($senhaUsuario)
    {
        $this->senhaUsuario = $senhaUsuario;
    }

    /**
     * @return mixed
     */
    public function getNomeCompletoUsuario()
    {
        return $this->nomeCompletoUsuario;
    }

    /**
     * @param mixed $nomeCompletoUsuario
     */
    public function setNomeCompletoUsuario($nomeCompletoUsuario)
    {
        $this->nomeCompletoUsuario = $nomeCompletoUsuario;
    }

    /**
     * @return mixed
     */
    public function getTelefoneUsuario()
    {
        return $this->telefoneUsuario;
    }

    /**
     * @param mixed $telefoneUsuario
     */
    public function setTelefoneUsuario($telefoneUsuario)
    {
        $this->telefoneUsuario = $telefoneUsuario;
    }

    /**
     * @return mixed
     */
    public function getCpfUsuario()
    {
        return $this->cpfUsuario;
    }

    /**
     * @param mixed $cpfUsuario
     */
    public function setCpfUsuario($cpfUsuario)
    {
        $this->cpfUsuario = $cpfUsuario;
    }


}
