<?php

require_once 'connection.php';
require_once 'usuario.php';

class dao
{
    private $db;

    /**
     * usuarioDAO constructor.
     */
    public function __construct(){
        try{
            $this->db = new DB_CONNECT();
        }catch (Exception $ex){
            echo $ex;
        }
    }

    function VerificaUsuario($usuarioRequest){
        $conn = $this->db->getConnection();
        
        $query = "Select * from `Usuario` where `nomeUsuario` = ? and `senhaUsuario` = ?";

        $stm = new PDOStatement();
        $stm = $conn->prepare($query);
        $stm->bindValue(1,$usuarioRequest->getNomeUsuario());
        $stm->bindValue(2,$usuarioRequest->getSenhaUsuario());
        $stm->execute();

        $usuarioReturn = null;

        while($linha = $stm->fetch(PDO::FETCH_OBJ)){
            $usuarioReturn = new usuario();
            $usuarioReturn->setId($linha->id);
            $usuarioReturn->setNomeUsuario($linha->nomeUsuario);
            $usuarioReturn->setSenhaUsuario($linha->senhaUsuario);
            $usuarioReturn->setNomeCompletoUsuario($linha->nomeCompletoUsuario);
            $usuarioReturn->setTelefoneUsuario($linha->telefoneUsuario);
            $usuarioReturn->setCpfUsuario($linha->cpfUsuario);
        }

        return $usuarioReturn;

    }

    function Script(){
        $conn = $this->db->getConnection();

        $query = "Select * from `CTO`";

        $stm = new PDOStatement();
        $stm1 = new PDOStatement();
        $stm = $conn->prepare($query);
        $stm->execute();

        while($linha = $stm->fetch(PDO::FETCH_OBJ)){
            $id = $linha->id;
            $pppoe = $linha->loginCliente;
            $cto = $linha->codCTO;

            $cortaCTO = explode('.',$cto);

            if(is_numeric($cortaCTO[3])){
                $temp_cx = strval(intval($cortaCTO[3]));

                switch (strlen($temp_cx)){
                    case 1:
                        $cto = strval(intval($cortaCTO[0])).'.'.$cortaCTO[1].'.'.$cortaCTO[2].'.00'.$temp_cx.'A';
                        break;

                    case 2:
                        $cto = strval(intval($cortaCTO[0])).'.'.$cortaCTO[1].'.'.$cortaCTO[2].'.0'.$temp_cx.'A';
                        break;

                    case 3:
                        $cto = strval(intval($cortaCTO[0])).'.'.$cortaCTO[1].'.'.$cortaCTO[2].'.'.$temp_cx.'A';
                        break;

                }
            }else{
                $cto = substr($cto,1);
            }

            if(substr($pppoe,0,5) == "stlk_"){
                $pppoe = substr($pppoe,5,-6).'@stlk';
            }

            if($pppoe == 'ocupado@innon' || $pppoe == 'verificar@innon'){
                $pppoe = 'ocupado';
            }

            $grava = "Update `CTO` set codCTO=? where id=?";

            $stm1 = $conn->prepare($grava);
            $stm1->bindValue(1,$cto);
            $stm1->bindValue(2,$id);
            $stm1->execute();

            $err = $stm1->errorInfo();

            echo $err[1]."\n";



        }

    }


}