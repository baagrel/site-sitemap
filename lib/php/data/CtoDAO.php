<?php

require_once 'connection.php';

class CtoDAO
{
    private $db;


    public function __construct(){
        try{
            $this->db = new DB_CONNECT();
        }catch (Exception $ex){
            echo $ex;
        }
    }

    function GravaCto($cto,$pt,$cliente,$rx){

        $conn = $this->db->getConnection();

        $query = "Select * from `CTO` WHERE `codCTO` = ? and `porta` = ?";


        $stm = new PDOStatement();
        $stm = $conn->prepare($query);

        $stm->bindValue(1,$cto);
        $stm->bindValue(2,$pt);
        $stm->execute();

        $existente = false;
        while($linha = $stm->fetch(PDO::FETCH_OBJ)){
            $existente = true;
            break;
        }


        if($existente){
            $grava = "Update `CTO` set loginCliente= ?, rxSinal= ? where codCTO= ? and porta= ?";
        }else{
            $grava = "Insert into `CTO` (loginCliente, rxSinal, codCTO, porta) values (?,?,?,?)";
        }

        $stm = $conn->prepare($grava);
        $stm->bindValue(1,$cliente);
        $stm->bindValue(2,$rx);
        $stm->bindValue(3,$cto);
        $stm->bindValue(4,$pt);
        $stm->execute();

        return $this->BuscaCto($cto,'','');


    }


    function BuscaCto($cto,$cliente,$rx){
        $conn = $this->db->getConnection();

        $query = "SELECT * FROM `CTO` WHERE";

        if($cto != ""){
            $cto = '%'.$cto.'%';
            $query = $query." `codCTO` like ? AND";
        }
        if($cliente != ""){
            $cliente = '%'.$cliente.'%';
            $query = $query." `loginCliente` like ? AND";
        }
        if($rx != ""){
            $query = $query." `rxSinal` < ?";
        }


        if(substr($query,-5) == "WHERE"){
            $query = substr($query,0,strlen($query) -5);
        }
        if(substr($query, -3) == "AND"){
            $query =  substr($query, 0,strlen($query) -3);
        }

        $query = $query . " ORDER BY `codCTO`, `porta` ASC";

        $stm = new PDOStatement();
        $stm = $conn->prepare($query);
        $c = 0;

        if($cto != ""){
            $c = $c + 1;
            $stm->bindValue($c ,$cto);
        }
        if($cliente != ""){
            $c = $c + 1;
            $stm->bindValue($c ,$cliente);
        }
        if($rx != ""){
            $c = $c + 1;
            $stm->bindValue($c ,$rx);
        }

        $stm->execute();
        $str = "";
        $itens = 0;

        while($linha = $stm->fetch(PDO::FETCH_OBJ)){
            $rx = $linha->rxSinal;

            if($rx < - 23 || $rx == 'L0'){
                $str = $str."<tr class='bg-danger'>
                <td class='col-xs-2' id='codCTO'>$linha->codCTO</td>
                <td class='col-xs-2' id='porta'>$linha->porta</td>
                <td class='col-xs-6' id='loginCliente'>$linha->loginCliente</td>
                <td class='col-xs-2' id='rxSinal'>$linha->rxSinal</td>
                </tr>";
            }else{
                $str = $str."<tr>
                <td class='col-xs-2' id='codCTO'>$linha->codCTO</td>
                <td class='col-xs-2' id='porta'>$linha->porta</td>
                <td class='col-xs-6' id='loginCliente'>$linha->loginCliente</td>
                <td class='col-xs-2' id='rxSinal'>$linha->rxSinal</td>
                </tr>";
            }
            $itens = $itens + 1;
        }
        return $str.'|||'.$itens;
    }


}