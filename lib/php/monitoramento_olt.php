<?php

require_once 'data/ClienteDAO.php';
require_once 'data/RamalDAO.php';
require_once 'data/slotDAO.php';
require_once 'data/oltDao.php';

if($_SERVER["REQUEST_METHOD"] == "POST") {
    if ($_POST['action'] == "request_pons") {
        BuscaPon();
    }else if($_POST['action'] == "send_pons"){
        SendPon();
    }else if($_POST['action'] == "monitoramento"){
        Monitoramento();
    }else if($_POST['action'] == "ramal_list"){
        RamalList();
    }else if($_POST['action'] == "request_pons_v2"){
        BuscaPonV2();
    }else if($_POST['action'] == "send_pons_v2"){
        SendPonsV2();
    }else if($_POST['action'] == "monitoramento_v2"){
        MonitoramentoV2();
    }else if($_POST['action'] == "busca_historico"){
        BuscaHistorico();
    }
}

function BuscaPon(){
    $daoClienteCto = new ClienteDAO();
    echo $daoClienteCto->BuscaPons($_POST['olt']);
}

function SendPon(){
    $daoCLienteCto = new ClienteDAO();
    echo $daoCLienteCto->DecodeAndSave($_POST['data']);
}

function Monitoramento(){
    $olt = $_POST['olt'];
    $slot = $_POST['slot'];
    $pon = $_POST['pon'];

    $daoCLienteCto = new ClienteDAO();
    echo $daoCLienteCto->Busca($olt, $slot, $pon);
}

function RamalList(){
    $ramalDao = new RamalDAO();
    echo $ramalDao->RamalList();
}

function BuscaPonV2(){
    $slotDao = new slotDAO();
    echo $slotDao->BuscaSlots($_POST['olt']);
}

function SendPonsV2(){
    $oltDao = new oltDao();
    echo $oltDao->decode_and_save($_POST['data']);
}

function MonitoramentoV2(){
    $oltDao = new oltDao();
    echo $oltDao->monitoramento($_POST['olt'],$_POST['slot']);
}

function BuscaHistorico(){
    $oltDao = new oltDao();
    echo $oltDao->BuscaHIstorico();
}