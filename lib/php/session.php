<?php
    session_start();

    require_once 'data/usuario.php';
    require_once 'data/dao.php';

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        if($_POST['action'] == "session-status"){
            SessionStatus();
        }elseif ($_POST['action'] == "session-start"){
            Login();
        }elseif ($_POST['action'] == "session-close"){
            SessionClose();
        }

    }

    function Login(){
        $usuarioReturn = LoginVerify($_POST['user'],$_POST['pass']);
        if($usuarioReturn != null){
            SaveLogin($usuarioReturn);
        }else{
            echo '0';
        }
    }



    function SessionStatus(){
        if(session_status() == PHP_SESSION_ACTIVE){
           $usuarioReturn = LoginVerify($_SESSION['nomeUsuario'],$_SESSION['senhaUsuario']);
           if($usuarioReturn != null){
               echo $_SESSION['nomeCompletoUsuario'];
           }else{
               echo '0';
           }
        }else{
            echo '0';
        }
    }

    function LoginVerify($user,$pass){
        $usuarioRequest = new usuario();

        $usuarioRequest->setNomeUsuario($user);
        $usuarioRequest->setSenhaUsuario($pass);

       $dao = new dao();
       $usuarioReturn = $dao->VerificaUsuario($usuarioRequest);

        return $usuarioReturn;
    }

    function SaveLogin($usuarioReturn){
        $_SESSION['idUsuario'] = $usuarioReturn->getId();
        $_SESSION['nomeUsuario'] = $usuarioReturn->getNomeUsuario();
        $_SESSION['senhaUsuario'] = $usuarioReturn->getSenhaUsuario();
        $_SESSION['nomeCompletoUsuario'] = $usuarioReturn->getNomeCompletoUsuario();

        echo $_SESSION['nomeCompletoUsuario'];
    }



    function SessionClose(){
        if(session_destroy()){
            echo '1';
        }else{
            echo '0';
        }
    }

