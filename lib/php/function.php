<?php
require_once 'data/usuario.php';
require_once 'data/dao.php';
require_once 'data/AtendimentoDAO.php';
require_once 'data/CtoDAO.php';
require_once 'data/oltDao.php';

if($_SERVER["REQUEST_METHOD"] == "POST") {
    if ($_POST['action'] == "busca-cto") {
        BuscaCto();
    } else if ($_POST['action'] == "grava-cto") {
        GravaCto();
    } else if ($_POST['action'] == "grava-ate") {
        GravaAtendimento();
    } else if ($_POST['action'] == "busca-ate") {
        BuscaAtendimento();
    } else if ($_POST['action'] == "remove-ate") {
        RemoveAtendimento();
    } else if ($_POST['action'] == "busca-ocupacao"){
        BuscaOcupacao();
    }

}

function BuscaCto(){
    $cto_dao = new CtoDAO();
    echo $cto_dao->BuscaCto($_POST['cto'],$_POST['cliente'],$_POST['rx']);
}

function GravaCto(){
    $cto = $_POST['cto'];
    $pt = $_POST['pt'];
    $cliente = $_POST['cliente'];
    $rx = $_POST['rx'];

    $cto_dao = new CtoDAO();
    echo $cto_dao->GravaCto($cto,$pt,$cliente,$rx);
}

function BuscaAtendimento(){
    $data = $_POST['data'];
    $cliente = $_POST['cliente'];
    $assunto = $_POST['assunto'];
    $status = $_POST['status'];

    $atendimento_dao = new AtendimentoDAO();
    echo $atendimento_dao->BuscaAtendimento($cliente,$data,$assunto,$status);

}

function GravaAtendimento(){
    $id = $_POST['id'];
    $solicitante = $_POST['solicitante'];
    $pppoe = $_POST['pppoe'];
    $data = $_POST['data'];
    $hora = $_POST['hora'];
    $contato = $_POST['contato'];
    $plataforma = $_POST['plataforma'];
    $protocolo = $_POST['protocolo'];
    $assunto = $_POST['assunto'];
    $status = $_POST['status'];
    $tratativas = $_POST['tratativas'];
    $usuarioCadastro = $_POST['usuarioCadastro'];
    $horaCadastro = $_POST['horaCadastro'];

    $atendimento_dao = new AtendimentoDAO();
    echo $atendimento_dao->GravaAtendimento(
        $id,
        $solicitante,
        $pppoe,
        $data,
        $hora,
        $contato,
        $plataforma,
        $protocolo,
        $assunto,
        $status,
        $tratativas,
        $usuarioCadastro,
        $horaCadastro);
}

function BuscaOcupacao(){
    $oltDao = new oltDao();
    echo $oltDao->BuscaOcupacao();
}

