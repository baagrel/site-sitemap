<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!-- Responsivo -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../lib/css/styles.css">

    <!-- JavaScript -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script type="text/javascript" src="../lib/js/operacional.js"></script>
    <script src="https://igorescobar.github.io/jQuery-Mask-Plugin/js/jquery.mask.min.js"></script>

    <!--Título e Logo da Página-->
    <title>SiteMAP - Operacional</title>
    <link rel="shortcut icon" href="../img/title.png" type="image/x-icon" />

</head>
<body class="text-center">
<!-- Barra de Navegação-->
<header>
    <nav class="navbar navbar-light bg-light fixed-top flex-md-nowrap p-0 shadow" id="navBar">
        <!-- Logo e Nome do Site -->
        <a class="navbar-brand ml-3" href="#Home">
            <img src="../img/title.png" width="35" height="30" class="d-inline-block align-top" alt="">
            SiteMAP
        </a>

        <!-- Funções lado direito NavBar -->
        <form class="form-inline mr-3 m-1">
            <label class="mr-4 invisible" id="lbl_usuario_logado"></label>

            <!-- Botão de Login -->
            <button class="btn  btn-primary my-2 my-sm-0 " type="submit" id="btn_login_nav">Login</button>
        </form>

    </nav>
</header>

<div class="container-fluid">
    <div class="row">
        <!-- Side Nav-->
        <nav class=" col-md-2 list-group bg-light sidebar">
            <a class="list-group-item list-group-item-action" href="#" id="nav-operacional">Operacional</a>
            <a class="list-group-item list-group-item-action" href="#" id="nav-cliente">Cliente</a>
            <a class="list-group-item list-group-item-action" href="#" id="nav-olt">Ramal</a>
            <a class="list-group-item list-group-item-action" href="../index.php">Home</a>
        </nav>


        <!-- Tela Principal -->
        <div class="col-md-10 ml-sm-auto col-lg-10 px-4 main" role="main">
            <main class="align-self-center" id="main">

                <!-- Operacional -->
                <div id="form-operacional">
                    <div class="vertical-center">
                        <img src="../img/logo-1.png">
                    </div>
                </div>

                <!-- Form Cliente -->
                <div id="form-cliente">
                    <!-- Busca -->
                    <div id="cli-busca" class="d-none">
                        <form class="container-fluid clearfix">
                            <div class="input-group m-2">
                                <!-- Filtro PPPoE -->
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="txt_cli_busca_pppoe">PPPoE</label>
                                </div>
                                <input type="text" id="txt_cli_busca_pppoe" class="form-control col-3">

                                <!-- Filtro FHTT -->
                                <div class="input-group-prepend ml-2">
                                    <label class="input-group-text" for="txt_cli_busca_fhtt">FHTT</label>
                                </div>
                                <input type="text" id="txt_cli_busca_fhtt" class="form-control col-3">

                                <!-- Filtro Ramal -->
                                <div class="input-group-prepend ml-2">
                                    <label class="input-group-text" for="opt_cli_busca_ramal">Ramal</label>
                                </div>
                                <select class="form-control col-2" id="opt_cli_busca_ramal"></select>

                                <!-- Buscar -->
                                <button class="btn btn-primary ml-2 col-2" id="btn_cli_busca_buscar" >Buscar</button>

                                <!-- Lista -->
                                <div class="table-responsive m-1 tab-height">
                                    <table class="table table-sm table-striped" id="table_cliente">
                                        <thead class="thead-light">
                                        <tr>
                                            <th class="d-none">ID</th>
                                            <th>PPPoE</th>
                                            <th>Ramal</th>
                                            <th>Slot</th>
                                            <th>Pon</th>
                                            <th>Onu ID</th>
                                            <th class="d-none">Online</th>
                                            <th>FHTT</th>
                                        </tr>
                                        </thead>
                                        <tbody id="table_cli_busca">

                                        </tbody>
                                    </table>
                                </div>
                                <div class="container-fluid m-2">
                                    <!-- Contagem de Itens -->
                                    <label class="float-left" id="cli-listados"> </label>

                                    <!-- Botões -->
                                    <div class="float-right">
                                        <div class="form-row">
                                            <button class="btn btn-danger m-1 disabled" id="btn_cli_busca_remove" style="width: 100px">Remover</button>
                                            <button class="btn btn-warning m-1 disabled" id="btn_cli_busca_alterar" style="width: 100px">Alterar</button>
                                            <button class="btn btn-success m-1" id="btn_cli_busca_adicionar" style="width: 100px">Adicionar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <!-- Cadastro -->
                    <div id="cli-cadastro">
                        <form class="container-fluid clearfix">
                            <div class="input-group form-row m-2 pr-1">
                                <!-- ID -->
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for='txt_cli_cad_id' style="width: 100px">ID</label>
                                </div>
                                <input type="text" id="txt_cli_cad_id disabled" name="id" class="form-control col-1">
                            </div>

                            <div class="input-group form-row m-2 mt-2 pr-1">
                                <!-- PPPoE -->
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for='txt_cli_cad_pppoe' style="width: 100px">PPPoE</label>
                                </div>
                                <input type="text" id="txt_cli_cad_pppoe" name="pppoe" class="form-control col-5">

                                <!-- FHTT -->
                                <div class="input-group-prepend ml-2">
                                    <label class="input-group-text" for='txt_cli_cad_fhtt' style="width: 100px">FHTT</label>
                                </div>
                                <input type="text" id="txt_cli_cad_fhtt" name="fhtt" class="form-control col-5">
                            </div>

                            <div class="input-group form-row m-2 mt-2 pr-2">
                                <!-- Ramal -->
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="txt_cli_cad_ramal" style="width: 100px;">Ramal</label>
                                </div>
                                <input type="text" id="txt_cli_cad_ramal" name="ramal" class="form-control col-2 disabled">

                                <!-- Slot -->
                                <div class="input-group-prepend ml-2">
                                    <label class="input-group-text" for="txt_cli_cad_slot" style="width: 100px">Slot</label>
                                </div>
                                <input type="text" id="txt_cli_cad_slot" name="slot" class="form-control col-2 disabled">

                                <!-- Pon -->
                                <div class="input-group-prepend ml-2">
                                    <label class="input-group-text" for="txt_cli_cad_pon" style="width: 100px">PON</label>
                                </div>
                                <input type="text" id="txt_cli_cad_pon" name="pon" class="form-control col-2 disabled">

                                <!-- Onu Id -->
                                <div class="input-group-prepend ml-2">
                                    <label class="input-group-text" for="txt_cli_cad_onuid" style="width: 100px">ONU ID</label>
                                </div>
                                <input type="text" id="txt_cli_cad_onuid" name="onuid" class="form-control col-2 disabled">
                            </div>
                            <div class="input-group form-row m-2 mt-2 pr-2">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="txt_cli_cad_onustatus" style="width: 100px">ONU</label>
                                </div>
                                <input type="text" id="txt_cli_cad_onustatus" name="OnuStatus" class="bg-danger form-control col-2 disabled">
                            </div>
                        </form>
                    </div>
                </div>

                <!-- Form OLT -->
                <div id="form-olt" class="d-none ">

                </div>

            </main>
        </div>
    </div>
</div>

</body>
</html>

<!-- Estrutura Dialog -->
<div class="modal fade" id="dialog-master" data-backdrop="static" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="dialog-title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="dialog-body"

        </div>
    </div>
</div>
</div>

