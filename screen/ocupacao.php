<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!-- Responsivo -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../lib/css/styles.css">

    <!-- JavaScript -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://igorescobar.github.io/jQuery-Mask-Plugin/js/jquery.mask.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js" integrity="sha256-Uv9BNBucvCPipKQ2NS9wYpJmi8DTOEfTA/nH2aoJALw=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="../lib/js/ocupacao.js"></script>


    <!--Título e Logo da Página-->
    <title>SiteMAP - Monitoramento</title>
    <link rel="shortcut icon" href="../img/title.png" type="image/x-icon" />

</head>
<body class="text-center">
<!-- Barra de Navegação-->
<header>
    <nav class="navbar navbar-light bg-light fixed-top flex-md-nowrap p-0 shadow" id="navBar">
        <!-- Logo e Nome do Site -->
        <a class="navbar-brand ml-3" href="#">
            <img src="../img/title.png" width="35" height="30" class="d-inline-block align-top" alt="">
            SiteMAP
        </a>
    </nav>
</header>

<div class="container-fluid">
    <div class="main pt-5">

        <h1 class="align-content-center mt-1 mb-2"> Quantidade de Clientes por Porta PON</h1>

        <div class="table-responsive m-0 tab-height">
            <table class="table table-bordered" id="table_ocupacao">
                <thead class="">
                <tr class="bg-dark text-white">
                    <th>Ramal - Slot</th>
                    <th>PON1</th>
                    <th>PON2</th>
                    <th>PON3</th>
                    <th>PON4</th>
                    <th>PON5</th>
                    <th>PON6</th>
                    <th>PON7</th>
                    <th>PON8</th>
                    <th>PON9</th>
                    <th>PON10</th>
                    <th>PON11</th>
                    <th>PON12</th>
                    <th>PON13</th>
                    <th>PON14</th>
                    <th>PON15</th>
                    <th>PON16</th>
                </tr>
                </thead>
                <tbody id="table_body_ocupacao">

                </tbody>
            </table>
        </div>
    </div>
</div>

</body>

</html>

