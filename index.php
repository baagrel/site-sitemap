<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!-- Responsivo -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="lib/css/styles.css">

    <!-- JavaScript -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script type="text/javascript" src="lib/js/functions.js"></script>
    <script src="https://igorescobar.github.io/jQuery-Mask-Plugin/js/jquery.mask.min.js"></script>

    <!--Título e Logo da Página-->
    <title>INNON - SiteMAP</title>
    <link rel="shortcut icon" href="img/title.png" type="image/x-icon" />

</head>
<body class="text-center">
<!-- Barra de Navegação-->
    <header>
        <nav class="navbar navbar-light bg-light fixed-top flex-md-nowrap p-0 shadow" id="navBar">
            <!-- Logo e Nome do Site -->
            <a class="navbar-brand ml-3" href="">
                <img src="img/title.png" width="35" height="30" class="d-inline-block align-top" alt="">
                INNON - SiteMAP
            </a>


            <img src="img/logo-1.png" height="30" class="d-inline-block align-top navbar-brand ml-3 align-content-center" alt="">



            <!-- Funções lado direito NavBar -->
            <form class="form-inline mr-3 m-1">
                <label class="mr-4 invisible" id="lbl_usuario_logado">Teste</label>

                <!-- Botão de Login -->
                <button class="btn  btn-primary my-2 my-sm-0 " type="submit" id="btn_login_nav">Login</button>
            </form>

        </nav>
    </header>

    <div class="container-fluid">
        <div class="row">
            <!-- Side Nav-->
            <nav class=" col-md-2 list-group bg-light sidebar">
                <a class="list-group-item list-group-item-action" href="#" id="nav-home">Home</a>
                <a class="list-group-item list-group-item-action" href="#" id="nav-cto">CTOs</a>
                <a class="list-group-item list-group-item-action d-none" href="#" id="nav-atendimento">Atendimentos</a>
                <a class="list-group-item list-group-item-action d-none" href="screen/operacional.php" id="nav-operacional">Operacional</a>
                <a class="list-group-item list-group-item-action" href="screen/monitoramento.php" id="nav-monitor">Monitoramento</a>
                <a class="list-group-item list-group-item-action" href="screen/ocupacao.php" id="nav-ocupacao">Ocupação OLT</a>
            </nav>


            <!-- Tela Principal -->
            <div class="col-md-10 ml-sm-auto col-lg-10 px-4 main" role="main">
                <main class="align-self-center" id="main">

                    <!-- Home -->
                    <div id="form-home">
                        <div class="vertical-center">

                        </div>
                    </div>

                    <!-- FormCTO -->
                    <div id="form-cto" class="d-none ">
                        <div class="input-group m-2">
                            <!-- Nome CTO !-->
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="txt_id_cto">CTO</label>
                            </div>
                            <input type="text" id="txt_id_cto" class="form-control col-3">

                            <!-- Porta -->
                            <div class="input-group-prepend ml-1">
                                <label class="input-group-text" for="txt_num_porta">Porta</label>
                            </div>
                            <input type="text" id="txt_num_porta" class="form-control col-1 w-25">

                            <!-- Cliente -->
                            <div class="input-group-prepend ml-1">
                                <label class="input-group-text" for="txt_cliente">Cliente</label>
                            </div>
                            <input type="text" id="txt_cliente" class="form-control col-2">
                            <div class="input-group-append">
                                <select class="input-group-text" id="cmb_sufix_cto" for="txt_cliente">
                                    <option value=""></option>
                                    <option value="@stlk">@stlk</option>
                                    <option value="@innon">@innon</option>
                                </select>
                            </div>

                            <!-- RX -->
                            <div class="input-group-prepend ml-1">
                                <label class="input-group-text" for="txt_rx_sinal">RX</label>
                            </div>
                            <input type="text" id="txt_rx_sinal" class="form-control col-1">

                            <button class="btn btn-success ml-2" id="btn_gravar_cto">Salvar</button>
                            <button class="btn btn-primary ml-2" id="btn_buscar_cto"">Buscar</button>
                            <button class="btn btn-warning ml-2" id="btn_limpa_cto">Limpar</button>
                        </div>

                        <div class="table-responsive m-0 tab-height">
                            <table class="table table-sm table-hover table-bordered table-striped" id="table_cto">
                                <thead class="">
                                <tr>
                                    <th class="col-xs-2">CTO</th>
                                    <th class="col-xs-2">Porta</th>
                                    <th class="col-xs-6">Cliente</th>
                                    <th class="col-xs-2">RX</th>
                                </tr>
                                </thead>
                                <tbody id="table_body_cto">

                                </tbody>
                            </table>
                        </div>

                        <!-- Contador de Linhas -->
                        <div class="row m-2">
                            <label id="cto-listados"></label>
                        </div>


                    </div>

                    <!-- Form Atendimentos -->
                    <div id="form-atendimento" class="d-none ">
                        <!-- Busca -->
                        <div id="ate-busca" class="d-none">
                            <div class="input-group m-2">
                                <!-- Filtro Data -->
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="txt_data_atendimento">Data</label>
                                </div>
                                <input type="text" id="txt_data_atendimento" class="form-control col-2">

                                <!-- Filtro Cliente -->
                                <div class="input-group-prepend ml-2">
                                    <label class="input-group-text" for="txt_cliente_Atendimento">Cliente</label>
                                </div>
                                <input type="text" id="txt_cliente_Atendimento" class="form-control col-4">

                                <!-- Filtro Assunto -->
                                <div class="input-group-prepend ml-2">
                                    <label class="input-group-text" for="opt_assunto_atendimento">Assunto</label>
                                </div>
                                <select id="opt_assunto_atendimento" name="assunto" class="form-control custom-select dropdown-content" >
                                    <option value=""></option>
                                    <option value="Wi-Fi">Wi-Fi</option>
                                    <option value="L0">L0</option>
                                    <option value="Oscilação de Conexão">Oscilação de Conexão</option>
                                    <option value="Alteração de Senha Wi-Fi">Alteração de Senha Wi-Fi</option>
                                    <option value="Rompimento de Fibra">Rompimento de Fibra</option>
                                    <option value="Vandalismo na rede externa">Vandalismo na rede externa</option>
                                    <option value="Informações">Informações</option>
                                    <option value="Financeiro">Financeiro</option>
                                    <option value="Vendas">Vendas</option>
                                    <option value="Agendamento">Agendamento</option>
                                    <option value="Outro">Outro</option>
                                </select>

                                <!-- Filtro Status -->
                                <div class="input-group-prepend ml-1">
                                    <label class="input-group-text" for="opt_status_atendimento">Status</label>
                                </div>
                                <select class="form-control col-2" id="txt_status_atendimento">
                                    <option value=""></option>
                                    <option value="Aberto">Aberto</option>
                                    <option value="Fechado">Fechado</option>
                                </select>

                                <button class="btn btn-primary ml-2" id="btn_busca_atendimento" style="width: 100px">Buscar</button>
                            </div>

                            <div class="table-responsive m-1 tab-height">
                                <table class="table table-sm table-striped " id="table_atendimento">
                                    <thead class="thead-light">
                                    <tr>
                                        <th class="d-none">Id</th>
                                        <th>Solicitante</th>
                                        <th>PPPoE Cliente</th>
                                        <th>Data</th>
                                        <th>Horário</th>
                                        <th class="d-none">Contato</th>
                                        <th class="d-none">Plataforma</th>
                                        <th>Assunto</th>
                                        <th class="d-none">Protocolo</th>
                                        <th class="d-none">Tratativas</th>
                                        <th>Status</th>
                                        <th class="d-none">Usuario Cadastro</th>
                                        <th class="d-none">Hora Cadastro</th>
                                    </tr>
                                    </thead>
                                    <tbody id="table_body_atendimento">

                                    </tbody>
                                </table>
                            </div>
                            <div class="m-2">
                                <!-- Contagem de Itens -->
                                <label class="float-left" id="ate-listados"></label>


                                <!-- Botões de Ação -->
                                <div class="float-right botao-site" >
                                    <div class="form-row">
                                        <button class="btn btn-danger m-1 disabled" id="btn_remover_ate" style="width: 100px">Remover</button>
                                        <button class="btn btn-warning m-1 disabled" id="btn_alterar_ate" style="width: 100px">Alterar</button>
                                        <button class="btn btn-success m-1" id="btn_incluir_ate" style="width: 100px">Adicionar</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Cadastro -->
                        <div id="ate-cadastro" class="d-none">
                            <form class="container-fluid clearfix">

                                <h2>Cadastro de Atendimentos</h2>

                                <div class="input-group form-row m-2 pr-1" >
                                    <!-- Solicitante -->
                                    <div class="input-group-prepend">
                                        <label class="input-group-text" for="txt_ate_solicitante" style="width: 100px">Solicitante:</label>
                                    </div>
                                    <input type="text" id="txt_ate_solicitante" name="Solicitante"class="form-control">

                                    <!-- PPPoE Cliente -->
                                    <div class="input-group-prepend ml-1">
                                        <label class="input-group-text" for="txt_ate_pppoe" style="width: 100px">PPPoE:</label>
                                    </div>
                                    <input type="text" id="txt_ate_pppoe" name="PPPoE"class="form-control ">
                                </div>

                                <div class="input-group form-row m-2 pr-1" >
                                    <!-- Data -->
                                    <div class="input-group-prepend">
                                        <label class="input-group-text" for="txt_ate_data" style="width: 100px">Data:</label>
                                    </div>
                                    <input type="text" id="txt_ate_data" name="Data"class="form-control ">

                                    <!-- Hora -->
                                    <div class="input-group-prepend ml-1">
                                        <label class="input-group-text" for="txt_ate_hora" style="width: 100px">Hora:</label>
                                    </div>
                                    <input type="text" id="txt_ate_hora" name="Hora"class="form-control ">

                                    <!-- Contato -->
                                    <div class="input-group-prepend ml-1">
                                        <label class="input-group-text" for="txt_ate_contato" style="width: 100px">Contato:</label>
                                    </div>
                                    <input type="text" id="txt_ate_contato" name="Contato"class="form-control ">

                                    <!-- Protocolo -->
                                    <div class="input-group-prepend ml-1">
                                        <label class="input-group-text" for="txt_ate_protocolo" style="width: 100px">Protocolo:</label>
                                    </div>
                                    <input type="text" id="txt_ate_protocolo" name="Protocolo"class="form-control ">
                                </div>

                                <div class="input-group form-row m-2 pr-1" >
                                    <!-- Plataforma -->
                                    <div class="input-group-prepend">
                                        <label class="input-group-text" for="opt_ate_plataforma" style="width: 100px">Plataforma:</label>
                                    </div>
                                    <select id="opt_ate_plataforma" name="plataforma" class="form-control custom-select dropdown-content" >
                                        <option value="URA">URA</option>
                                        <option value="Telefone">Telefone</option>
                                        <option value="WhatsApp">WhatsApp</option>
                                    </select>

                                    <!-- Assunto -->
                                    <div class="input-group-prepend ml-1">
                                        <label class="input-group-text" for="opt_ate_assunto" style="width: 100px">Assunto:</label>
                                    </div>
                                    <select id="opt_ate_assunto" name="assunto" class="form-control custom-select dropdown-content" >
                                        <option value="Wi-Fi">Wi-Fi</option>
                                        <option value="L0">L0</option>
                                        <option value="Oscilação de Conexão">Oscilação de Conexão</option>
                                        <option value="Alteração de Senha Wi-Fi">Alteração de Senha Wi-Fi</option>
                                        <option value="Rompimento de Fibra">Rompimento de Fibra</option>
                                        <option value="Vandalismo na rede externa">Vandalismo na rede externa</option>
                                        <option value="Informações">Informações</option>
                                        <option value="Financeiro">Financeiro</option>
                                        <option value="Vendas">Vendas</option>
                                        <option value="Agendamento">Agendamento</option>
                                        <option value="Outro">Outro</option>
                                    </select>

                                    <!-- Status -->
                                    <div class="input-group-prepend ml-1">
                                        <label class="input-group-text" for="opt_ate_status" style="width: 100px">Status:</label>
                                    </div>
                                    <select id="opt_ate_status" name="plataforma" class="form-control custom-select dropdown-content" >
                                        <option value="Aberto">Aberto</option>
                                        <option value="Fechado">Fechado</option>
                                    </select>
                                </div>

                                <div class="input-group  form-row m-2 pr-1" >
                                    <!-- Tratativas -->
                                    <div class="input-group-prepend">
                                        <label class="input-group-text" for="txt_ate_tratativas" style=" height:125px; width: 100px"> Tratativas:</label>
                                    </div>
                                    <textarea type="text" rows="2" id="txt_ate_tratativas" name="Tratativas" class="form-control" style=" height:125px;" ></textarea>
                                </div>

                                <div class="float-right">
                                    <button type="button" class="btn btn-danger m-1" id="btn_ate_cancelar" style="width: 100px;">Cancelar</button>
                                    <button type="button" class="btn btn-success m-1" id="btn_ate_salvar" style="width: 100px;">Gravar</button>
                                </div>

                            </form>
                        </div>
                    </div>

                </main>
            </div>
        </div>
    </div>

</body>
</html>

<!-- Estrutura Dialog -->
<div class="modal fade" id="dialog-master" data-backdrop="static" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="dialog-title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="dialog-body"

            </div>
        </div>
    </div>
</div>

